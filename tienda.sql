-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-02-2018 a las 17:24:03
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `descripcionCat` longtext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`, `descripcionCat`) VALUES
(1, 'Frutos Secos ', 'Categoria de frutos secos '),
(2, 'Vinagres', 'Categoria con productos de vinagres y similar'),
(3, 'Snacks', 'Patatas fritas y demas'),
(4, 'Cervezas', 'Cervezas diversas.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `nombreProducto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `precioProducto` float NOT NULL,
  `cantidadProducto` int(11) NOT NULL,
  `imagenProducto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaProducto` datetime NOT NULL,
  `descripcionProducto` longtext COLLATE utf8_spanish_ci NOT NULL,
  `idCat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombreProducto`, `precioProducto`, `cantidadProducto`, `imagenProducto`, `fechaProducto`, `descripcionProducto`, `idCat`) VALUES
(1, 'cacahuete de temporada', 4, 20, '', '2018-01-09 00:00:00', 'descripcion de los cacahuetes de temporada', 1),
(2, 'melones de villaconejos', 5, 30, '', '2018-01-09 00:00:00', 'Melones que te cagas', 2),
(3, 'datiles ', 6, 40, '', '2018-01-09 00:00:00', 'Datiles de Morocco.', 1),
(4, 'Higos', 3, 50, '', '2018-01-09 00:00:00', 'Higos a brevas.', 1),
(5, 'datiles', 13, 13, '', '0000-00-00 00:00:00', '', 0),
(12, 'Huevos de Caffarnaun', 100, 2, '', '0000-00-00 00:00:00', 'huevos', 3),
(13, 'prueba', 1, 2, '1516127327C:xampp	mpphpD05C.tmp', '0000-00-00 00:00:00', 'lalallala', 2),
(14, 'prueba2', 222, 22, '1516127446C:xampp	mpphp9EE3.tmp', '0000-00-00 00:00:00', 'lalalal2', 0),
(15, 'prueba3', 222, 1221, '1516127606sindien.jpg', '0000-00-00 00:00:00', 'lalalal3', 0),
(16, 'ndfañrekl', 6516, 0, '', '0000-00-00 00:00:00', 'cacasdcadsc', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `claveUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `sesionUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `correoUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `activadoUsuario` tinyint(1) NOT NULL DEFAULT '0',
  `codigoCorreoUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombreUsuario`, `claveUsuario`, `sesionUsuario`, `correoUsuario`, `activadoUsuario`, `codigoCorreoUsuario`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '9d810bf347999b370d8026af270f5fd6', '', 1, ''),
(2, 'lalal', '81dc9bdb52d04dc20036dbd8313ed055', '', 'lalal@gmail.com', 0, ''),
(3, 'pepe', '926e27eecdbc7a18858b3798ba99bddd', '', 'pepe@mongol.es', 0, ''),
(4, 'manolo', '12cdb9b24211557ef1802bf5a875fd2c', '', 'manolo@gmail.com', 1, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
